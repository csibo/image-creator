//object of the element to be moved
_item = null;
_previous_item = null;

_canvas = null;
_properties = null;

//stores x & y co-ordinates of the mouse pointer
mouse_x = 0;
mouse_y = 0;

//stores x & y co-ordinates of the mouse at the down event
mouse_down_x = 0;
mouse_down_y = 0;

mouse_move_threshold = 5;
threshold_reached = false;

handle_lock = false;
resize_handle_size = 15;

// stores top,left values (edge) of the element
ele_x = 0;
ele_y = 0;

id_number = 3;

//bind the functions
function move_init()
{
    document.onmousemove = OnMouseMove;
    document.onmouseup = OnMouseUp;
    document.onmousedown = OnMouseDown;
    
    _canvas = document.getElementById('canvas');
    _properties = document.getElementById('properties');
}

//destroy the object when we are done
function OnMouseUp()
{
    if(_item != null)
    {
        _previous_item = _item;
        _item = null;

        threshold_reached = false;
    }
    
    DisplayTemplate();
    
    handle_lock = false;
}

function OnMouseDown(e)
{
    mouse_down_x = document.all ? window.event.clientX : e.pageX;
    mouse_down_y = document.all ? window.event.clientY : e.pageY;
    
    handle_lock = true;
}

//main functions which is responsible for moving the element (div in our example)
function OnMouseMove(e)
{
    mouse_x = document.all ? window.event.clientX : e.pageX;
    mouse_y = document.all ? window.event.clientY : e.pageY;
    
    if(_item != null)
    {
        if(_item.style.cursor === "move")
        {
            if(!threshold_reached)
            {
                if(Math.sqrt(Math.exp(mouse_x - mouse_down_x, 2) + Math.exp(mouse_y - mouse_down_y, 2)) > mouse_move_threshold)
                    threshold_reached = true;
            }
            else
            {
                _item.style.left = (mouse_x - ele_x) + "px";
                _item.style.top = (mouse_y - ele_y) + "px";

                ValidateMove(_item);
            }
        }
        
        if(_item.style.cursor === "w-resize"  ||
           _item.style.cursor === "nw-resize" ||
           _item.style.cursor === "sw-resize")
        {
            if(mouse_x >= cumulativeOffset(_canvas).left)
            {
                var delta = _item.offsetLeft - (mouse_x - cumulativeOffset(_canvas).left);

                _item.style.width = (_item.offsetWidth + delta) + "px";
                _item.style.left = (_item.offsetLeft - delta) + "px";

                ValidateSize(_item);
            }
        }
        
        if(_item.style.cursor === "e-resize"  ||
           _item.style.cursor === "ne-resize" ||
           _item.style.cursor === "se-resize")
        {
            _item.style.width = (mouse_x - cumulativeOffset(_item).left) + "px";

            ValidateSize(_item);
        }
        
        if(_item.style.cursor === "n-resize"  ||
           _item.style.cursor === "nw-resize" ||
           _item.style.cursor === "ne-resize")
        {
            if(mouse_y >= cumulativeOffset(_canvas).top)
            {
                var delta = _item.offsetTop - (mouse_y - cumulativeOffset(_canvas).top);

                _item.style.height = (_item.offsetHeight + delta) + "px";
                _item.style.top = (_item.offsetTop - delta) + "px";

                ValidateSize(_item);
            }
        }
        
        if(_item.style.cursor === "s-resize"  ||
           _item.style.cursor === "sw-resize" ||
           _item.style.cursor === "se-resize")
        {
            _item.style.height = (mouse_y - cumulativeOffset(_item).top) + "px";

            ValidateSize(_item);
        }
        
        LoadImageProperties(_item);
    }
}

function ValidateMove(e)
{
    if(e.offsetLeft < 0)
        e.style.left = "0px";
    
    if(e.offsetTop < 0)
        e.style.top = "0px";
    
    if(e.offsetLeft + e.offsetWidth > _canvas.offsetWidth)
        e.style.left = (_canvas.offsetWidth - e.offsetWidth) + "px";
    
    if(e.offsetTop + e.offsetHeight > _canvas.offsetHeight)
        e.style.top = (_canvas.offsetHeight - e.offsetHeight) + "px";
}

function ValidateSize(e)
{
    if(e.offsetWidth <= 10)
        e.style.width = "10px";
    
    if(e.offsetHeight <= 10)
        e.style.height = "10px";
    
    if(e.offsetLeft + e.offsetWidth >= _canvas.offsetWidth)
        e.style.width = (_canvas.offsetWidth - e.offsetLeft) + "px";
    
    if(e.offsetTop + e.offsetHeight >= _canvas.offsetHeight)
        e.style.height = (_canvas.offsetHeight - e.offsetTop) + "px";
}

//will be called when user starts dragging an element
function SelectItem(ele)
{
    //store the object of the element which needs to be moved
    _item = ele;

    ele_x = mouse_x - _item.offsetLeft;
    ele_y = mouse_y - _item.offsetTop;

    _item.style.border = "1px dashed white";
    
    if(_item !== _previous_item)
        _previous_item.style.border = "none";

    LoadImageProperties(_item);
}

function SelectGlobal()
{
    if(_item === null)
    {
        LoadGlobalProperties();
        _previous_item.style.border = "none";
    }
}

function ChangeHandle(e)
{
    if(!handle_lock)
    {
        var hasLeftHandle = false;
        var hasRightHandle = false;
        var hasTopHandle = false;
        var hasBottomHandle = false;

        if(mouse_x > cumulativeOffset(e).left + e.offsetWidth - resize_handle_size)
            hasRightHandle = true;
        if(mouse_x < cumulativeOffset(e).left + resize_handle_size)
            hasLeftHandle = true;
        if(mouse_y > cumulativeOffset(e).top + e.offsetHeight - resize_handle_size)
            hasBottomHandle = true;
        if(mouse_y < cumulativeOffset(e).top + resize_handle_size)
            hasTopHandle = true;

        if(hasRightHandle && hasTopHandle)
            e.style.cursor = 'ne-resize';
        else if(hasLeftHandle && hasTopHandle)
            e.style.cursor = 'nw-resize';
        else if(hasRightHandle && hasBottomHandle)
            e.style.cursor = 'se-resize';
        else if(hasLeftHandle && hasBottomHandle)
            e.style.cursor = 'sw-resize';
        else if(hasRightHandle)
            e.style.cursor = 'e-resize';
        else if(hasLeftHandle)
            e.style.cursor = 'w-resize';
        else if(hasTopHandle)
            e.style.cursor = 'n-resize';
        else if(hasBottomHandle)
            e.style.cursor = 's-resize';
        else
            e.style.cursor = 'move';
    }
}

function ChangeItemProperty(property, value)
{
    switch(property)
    {
        case 'offsetLeft':
            _previous_item.style.left = (value) + "px";
            break;
        case 'offsetTop':
            _previous_item.style.top = (value) + "px";
            break;
        case 'offsetWidth':
            _previous_item.style.width = (value) + "px";
            break;
        case 'offsetHeight':
            _previous_item.style.height = (value) + "px";
            break;
        case 'zIndex':
            _previous_item.style.zIndex = value;
            break;
        case 'backgroundImage':
            _previous_item.style.backgroundImage = 'url("' + value + '")';
            break;
        case 'rotation':
            _previous_item.style.WebkitTransform = "rotate("+value+"deg)"; 
            _previous_item.style.msTransform = "rotate("+value+"deg)"; 
            _previous_item.style.transform = "rotate("+value+"deg)"; 
            break;
    }
    
    ValidateSize(_previous_item);
    ValidateMove(_previous_item);
    LoadImageProperties(_previous_item);
    DisplayTemplate();
}

function ChangeGlobalProperty(property, value)
{
    switch(property)
    {
        case 'offsetWidth':
            _canvas.style.width = (value) + "px";
            break;
        case 'offsetHeight':
            _canvas.style.height = (value) + "px";
            break;
    }
    
    LoadGlobalProperties();
    DisplayTemplate();
}

function LoadImageProperties(e)
{
    _properties.innerHTML = "<h3 style='text-align:center;'>Item Properties</h3><hr>\
    <table class='propertiesTable'>\
        <tr>\
            <td class='title'>X:</td>\
            <td class='input1'>\
                <input type='number' value='" + e.offsetLeft + "' onChange='ChangeItemProperty(\"offsetLeft\",this.value);'>\
                <select><option>px</option><option>%</option>\
            </td>\
        </tr>\
        <tr>\
            <td class='title'>Y:</td>\
            <td class='input1'>\
                <input type='number' value='" + e.offsetTop + "' onChange='ChangeItemProperty(\"offsetTop\",this.value);'>\
                <select><option>px</option><option>%</option>\
            </td>\
        </tr>\
        <tr>\
            <td class='title'>Z:</td>\
            <td class='input1'>\
                <input type='number' value='" + e.style.zIndex + "' onChange='ChangeItemProperty(\"zIndex\",this.value);'>\
            </td>\
        </tr>\
        <tr>\
            <td COLSPAN=99><hr></td>\
        </tr>\
        <tr>\
            <td class='title'>Width:</td>\
            <td class='input1'>\
                <input type='number' value='" + e.offsetWidth + "' onChange='ChangeItemProperty(\"offsetWidth\",this.value);'>\
                <select><option>px</option><option>%</option>\
            </td>\
        </tr>\
        <tr>\
            <td class='title'>Height:</td>\
            <td class='input1'>\
                <input type='number' value='" + e.offsetHeight + "' onChange='ChangeItemProperty(\"offsetHeight\",this.value);'>\
                <select><option>px</option><option>%</option>\
            </td>\
        </tr>\
        <tr>\
            <td COLSPAN=99><hr></td>\
        </tr>\
        <tr>\
            <td class='title'>Rotation:</td>\
            <td class='input1'>\
                <input type='number' value='" + e.style.transform.substr(7, e.style.transform.length - 11) + "' onChange='ChangeItemProperty(\"rotation\",this.value);'>\
                degrees\
            </td>\
        </tr>\
        <tr>\
            <td COLSPAN=99><hr></td>\
        </tr>\
        <tr>\
            <td class='title'>Image:</td>\
            <td  class='input1' COLSPAN=99><input type='text' value='" + e.style.backgroundImage.substr(5, e.style.backgroundImage.length - 7) + "' style='width:95%;' onChange='ChangeItemProperty(\"backgroundImage\",this.value);'></td>\
        </tr>\
        <tr>\
            <td COLSPAN=99><hr></td>\
        </tr>\
        <tr>\
            <td><button id='removeButton' onclick='RemoveItem(\""+e.id+"\");LoadGlobalProperties();'>Delete</button></td>\
        </tr>\
    </table>\
    ";
}

function LoadGlobalProperties()
{
    _properties.innerHTML = "<h3 style='text-align:center;'>Global Properties</h3><hr>\
    <table class='propertiesTable'>\
        <tr>\
            <td class='title'>Width:</td>\
            <td class='input1'>\
                <input type='number' value='" + _canvas.offsetWidth + "' onChange='ChangeGlobalProperty(\"offsetWidth\",this.value);'>\
                px\
            </td>\
        </tr>\
        <tr>\
            <td class='title'>Height:</td>\
            <td class='input1'>\
                <input type='number' value='" + _canvas.offsetHeight + "' onChange='ChangeGlobalProperty(\"offsetHeight\",this.value);'>\
                px\
            </td>\
        </tr>\
        <tr>\
            <td COLSPAN=99><hr></td>\
        </tr>\
        <tr>\
            <td class='title'>Output Format:</td>\
            <td class='input1'><select><option>PNG</option><option>JPG</option></td>\
        </tr>\
        <tr>\
            <td class='title'>Output Quality:</td>\
            <td class='input1'>\
                <input type='number' value='1000'>\
            </td>\
        </tr>\
        <tr>\
            <td COLSPAN=99><hr></td>\
        </tr>\
        <tr>\
            <td class='title'>Allow Overflow:</td>\
            <td class='input1'><input type='checkbox'></td>\
        </tr>\
    </table>\
    ";
}

function DisplayTemplate()
{
    var children = [].slice.call(_canvas.children);
    
    var template = 
'{\n\
    "settings":\n\
        {\n\
            "format": "png",\n\
            "quality": 1000,\n\
            "crop": 0,\n\
            "render_resolution":\n\
                {\n\
                    "width": '+_canvas.offsetWidth+',\n\
                    "height": '+_canvas.offsetHeight+'\n\
                },\n\
            "display_resolution":\n\
                {\n\
                    "width": '+_canvas.offsetWidth+',\n\
                    "height": '+_canvas.offsetHeight+'\n\
                }\n\
        },\n\
    "objects":\n\
        [\n';
    
    children.forEach(function(element){
        template += 
'            {\n\
                "type": "image",\n\
                "url": "'+element.style.backgroundImage.substr(5, element.style.backgroundImage.length - 7)+'",\n\
                "position":\n\
                    {\n\
                        "x": '+element.offsetLeft+',\n\
                        "y": '+element.offsetTop+'\n\
                    },\n\
                "size":\n\
                    {\n\
                        "width": '+element.offsetWidth+',\n\
                        "height": '+element.offsetHeight+'\n\
                    },\n\
                "angle": '+element.style.transform.substr(7, element.style.transform.length - 11)+'\n\
            },\n';
    });
    
    template = template.substr(0,template.length-2);
    
    template += '\n';
    
    template += 
'        ]\n\
}';
    
    document.getElementById('template').innerHTML = template;
}

function RemoveItem(id)
{
    document.getElementById(id).remove();
}

function AddImage()
{
    var newItem = document.createElement('div');
    newItem.id = 'ele' + id_number;
    newItem.class = 'canvasItem';
    newItem.onmousedown = function(){SelectItem(newItem);};
    newItem.onmousemove = function(){ChangeHandle(newItem);};
    newItem.style.width = "100px";
    newItem.style.height = "100px";
    newItem.style.backgroundColor = "red";
    newItem.style.backgroundSize = "100% 100%";
    newItem.style.zIndex = 5;
    newItem.style.transform = "rotate(0deg)";
    newItem.style.float = "left";
    newItem.style.position = "absolute";

    _canvas.appendChild(newItem);
    
    id_number++;
    
    LoadImageProperties(newItem);
}

function cumulativeOffset(element) {
    var top = 0, left = 0;
    do {
        top += element.offsetTop  || 0;
        left += element.offsetLeft || 0;
        element = element.offsetParent;
    } while(element);

    return {
        top: top,
        left: left
    };
};
<?php

require_once(__DIR__.'/../ImageCreator.php');

function CreateImage()
{
    set_time_limit(30);
    
    $output_path = './images/demo';

    $template = file_get_contents('example_template.json');
    
    $Creator = new ImageCreator($template);
    $path = $Creator->OutputImage($Creator->RenderImage(), $output_path);
    
    return $path;
}

?>

<!DOCTYPE html>
<html>
    <head>
        
        <script type="text/javascript" src="Demo/js/Demo.js"></script>
        
        <link rel="stylesheet" href="Demo/css/front_page.css">
        
    </head>
    
    <body onload="move_init();DisplayTemplate();SelectGlobal();">
        <div class="bounds_container"><bounds><div class="content_container">
                    
        <div class="panel">
        
            <div id="toolbar">
                <button onclick="SelectGlobal();">Global Settings</button>
                <button>Render Image</button>
                <button onclick="AddImage();" style="float:right;">Add Image</button>
                <button onclick="AddText();" style="float:right;">Add Text</button>
            </div>

            <div id="canvas" onmousedown="SelectGlobal();">
                <div id="ele1" class="canvasItem canvasImage" style="width:1200px;height:150px;background-image:url('Demo/images/sig_background.png');background-size:100% 100%;left:0px;top:0px;position:absolute;z-index:2;cursor:move;float:left;transform:rotate(0deg);" onmousedown="SelectItem(this);" onmousemove="ChangeHandle(this);"></div>
                <div id="ele2" class="canvasItem canvasImage" style="width:93px;height:107px;background-image:url('Demo/images/insignias/captain.png');background-size:100% 100%;left:24px;top:18px;position:absolute;z-index:3;cursor:move;float:left;transform:rotate(-20deg);" onmousedown="SelectItem(this);" onmousemove="ChangeHandle(this);"></div>
            </div>

            <div id="properties">
                Properties:
            </div>

            <?php //$path = CreateImage(); ?>
            <!--<img src="<?php echo $path; ?>">-->

            <div id="templateWrapper">
                <br><br>
                Template:
                <pre id="template">

    <?php echo file_get_contents('example_template.json'); ?>

                </pre>
            </div>

        </div>
                    
        </div></bounds></div>
        
    </body>
</html>